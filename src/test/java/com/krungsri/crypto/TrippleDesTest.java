package com.krungsri.crypto;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.krungsri.crypto.TrippleDes;

public class TrippleDesTest {

	private static final Logger log = LoggerFactory.getLogger(TrippleDesTest.class);

	private String merchantId = "950111534";
	private String merchantKey = "5ZKXizZA";
	private String sessionKey = "7n1t3zto";

	@Test
	public void shouldEncryptValidLength() throws Exception {

		// (1) get session key from BAY server
		// ex. merchantId and key
		// String merchantId = "950111534";
		// String merchantKey = "5ZKXizZA";

		// ... send HTTP POST to BAY server for get session key
		// ex. session key
		// String sessionKey = "7n1t3zto";
		log.debug("merchantId:{}, merchantKey:{}, sessionKey:{}", merchantId, merchantKey, sessionKey);

		// (2) setup message input (fixed length 300 bytes)
		StringBuilder msg = new StringBuilder();
		msg.append("0200EPAY50000000000000000000160250057774100218   000000000DirectAccount950111534       ");
		msg.append("0000000000000000000000000000-2AIS764009240010000000877741001               ");
		msg.append("00000000000000000000000007774100218      0170001081      ");
		msg.append("000000000000000000000000000001516000000000000000000000000000000000000000000000000");

		Assert.assertEquals(300, msg.length());

		// (4) encrypt messge
		String msgEncrypt = TrippleDes.encrypt(msg.toString(), sessionKey, merchantKey);
		log.debug("msg encrypt:{}", msgEncrypt);

		Assert.assertEquals(608, msgEncrypt.length());

	}

	//@Test
	public void shouldDecryptAsValidLength() throws Exception {

		// (5) send HTTP POST to BAY server and get response with encrypt message
		// ...

		StringBuilder msg = new StringBuilder();
		msg.append("5F5626F1D82EEB24D2E8A4DC7E43658CCFA559DFCC2A28CA7570924BDB752D0");
		msg.append("A89834594F4E3E4DFCB11BA78D1BB357402042D21FCBE71F6320D619C71A4EB8DA8A6");
		msg.append("4793D1DE31FC10C47D274C23F916FE3C677A94321667CFA559DFCC2A28CACFA559DFC");
		msg.append("C2A28CACFA559DFCC2A28CAC6C535908256836B7C62335D07A9A7A1339BDF6720C3EE");
		msg.append("46167217A32F5C9065E1C41D6A10E47D42365604B1ED36002C5692FEB5BF6491ACCFA");
		msg.append("559DFCC2A28CACFA559DFCC2A28CA54F6F4CB52F45D5745EDA9250BB3640B5A659DD4");
		msg.append("89CB4F4C54CDD3FEA3FB81D1A47CB138FD15D963CFA559DFCC2A28CACFA559DFCC2A2");
		msg.append("8CACFA559DFCC2A28CACEE8D216486FD3DBCFA559DFCC2A28CACFA559DFCC2A28CACF");
		msg.append("A559DFCC2A28CACFA559DFCC2A28CACFA559DFCC2A28CA8BC745156B91DF53");

		log.debug("msg length: {} bytes", msg.length());

		// (6) decrypt message
		String msgDecrypt = TrippleDes.decrypt(msg.toString(), sessionKey, merchantKey);
		log.debug("msgDecrypt:{}", msgDecrypt);
		Assert.assertEquals(300, msgDecrypt.length());

		String msgCode = msgDecrypt.substring(0, 4);
		log.debug("msgCode:{}", msgCode);

		Assert.assertEquals("0200", msgCode);

	}
}
