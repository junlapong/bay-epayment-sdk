package com.krungsri.epayment.util;

import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.krungsri.epayment.util.HttpUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HttpUtilsTest {

	private static final Logger log = LoggerFactory.getLogger(HttpUtilsTest.class);

	private static String TARGET_URL;

	@BeforeClass
	public static void setup() {

		log.info("***** SETUP *****");

		TARGET_URL = "https://int.krungsriepayment.com";

		HttpUtils.debugSSL(false, "ssl:handshake");
		HttpUtils.setupProxy("localhost", 3128);
	}

	@AfterClass
	public static void tearDown() {
		log.info("***** END *****");
	}

	@Test
	@Ignore
	public void shouldConnected() {

		try {

			HttpsURLConnection conn = (HttpsURLConnection) HttpUtils.getConnection(TARGET_URL, true);
			conn.connect();

			log.info("connected.");

			log.info(" SSL CIPHER : {}", conn.getCipherSuite());
			log.info(" REQ METHOD : {}", conn.getRequestMethod());
			log.info("RESP STATUS : {} {}", conn.getResponseCode(), conn.getResponseMessage());

			Assert.assertTrue(true);
		}
		catch (Exception e) {
			// e.printStackTrace();
			Assert.assertTrue(false);
		}
	}

	@Test
	@Ignore
	public void shouldNotConnected() {

		try {

			String noneExistingUrl = "https://www.somewhereonlyweknow.com";
			HttpsURLConnection conn = (HttpsURLConnection) HttpUtils.getConnection(noneExistingUrl, false);
			conn.connect();

			Assert.assertFalse(true);
		}
		catch (Exception e) {
			log.info("cannot connect.");
			log.info("fault positive: {}", e.getMessage());

			Assert.assertFalse(false);
		}
	}

	@Test
	@Ignore
	public void shouldSuportJvmVersion() {

		Properties prop = System.getProperties();
		for (String key : prop.stringPropertyNames()) {
			log.debug("{} : {}", key, prop.get(key));
		}

		String vendor = System.getProperty("java.vendor");
		String version = System.getProperty("java.specification.version");

		boolean isSupport = false;

		if (vendor != null && vendor.indexOf("IBM") > -1) {
			isSupport = Double.valueOf(version) >= 1.6;
		}
		else if (vendor != null && vendor.indexOf("Oracle") > -1) {
			isSupport = Double.valueOf(version) >= 1.7;
		}

		log.info("JVM: {} java.version: {} is support: {}", vendor, version, isSupport);

		Assert.assertTrue(isSupport);

	}
}
