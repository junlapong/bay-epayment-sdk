package com.krungsri.epayment.sdk;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.krungsri.epayment.sdk.InquiryTransactionStatusClient;
import com.krungsri.epayment.util.HttpUtils;

public class InquiryTransactionStatusClientTest {

	private static final Logger log = LoggerFactory.getLogger(InquiryTransactionStatusClientTest.class);

	static String SEVICE_URL;
	static String MERCHANTNUMBER;
	static String ORDERNUMBER;

	static InquiryTransactionStatusClient client = null;

	// ----------------------- SETUP ------------------------

	@BeforeClass
	public static void setup() {

		log.info("***** START *****");

		ResourceBundle config = ResourceBundle.getBundle("config.bay-epayment");

		SEVICE_URL = config.getString("epayment.service.url");
		MERCHANTNUMBER = config.getString("epayment.merchant.id");
		ORDERNUMBER = "1591706";

		HttpUtils.debugSSL(false, "ssl");
		// HttpUtils.setupProxy("localhost", 3128);

		client = new InquiryTransactionStatusClient();
		client.setServiceUri(SEVICE_URL);
	}

	@AfterClass
	public static void tearDown() {
		log.info("***** END *****");
	}

	// ----------------------- TEST ------------------------
	@Test
	@Ignore
	public void shouldGetTransactionStatus() {

		String status = client.getTransactionStatus(MERCHANTNUMBER, ORDERNUMBER);
		log.info("STATUS: {}", status);
		Assert.assertNotNull(status);
	}

}
