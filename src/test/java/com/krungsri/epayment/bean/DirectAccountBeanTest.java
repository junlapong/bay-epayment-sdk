package com.krungsri.epayment.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DirectAccountBeanTest {

	private static final Logger log = LoggerFactory.getLogger(DirectAccountBeanTest.class);

	@BeforeClass
	public static void setup() {
		log.info("***** START *****");
	}

	@AfterClass
	public static void tearDown() {
		log.info("***** END *****");
	}

	@Test
	@Ignore
	public void shouldGotValidLength() {

		String msg = getTestMessageInput();

		log.debug("MSG :\n\n{}\n", msg);
		log.debug("LEN : {} bytes", msg.length());

		Assert.assertEquals(300, msg.length());
	}

	private static String getTestMessageInput() {

		Date now = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");

		DirectAccountBean da = new DirectAccountBean();
		// 1 ex: 0200, 0700
		da.setTransactionMessageType(DirectAccountBean.TXN_REQ_TYPE);
		// 2
		da.setServiceType("EPAY");
		// 3.1 ex: 50, 35
		da.setTransactionCode(DirectAccountBean.TXN_BILLPAY);
		// 3.2
		da.setFromAccountCode("00");
		// 3.3
		da.setToAccountCode("00");
		// 4 ex: 20150825
		da.setTransactionDate(df.format(now));
		// 5
		da.setTransactionTime(tf.format(now));
		// 6.1
		da.setPanLength("16");
		// 6.2
		da.setPanNumber("0250057771345289   ");
		// 7.1
		da.setCvv("   ");
		// 7.2
		da.setExpiry("      ");
		// 8.1
		da.setPaymentType("DirectAccount");
		// 8.2 *****
		da.setAcquirerTerminalNumber("950111161       ");
		// 8.3 *****
		da.setAcquirerTerminalLocation("SBI                         ");
		// 8.4
		da.setAcquirerTerminalExp10("-2");
		// 8.5 *****
		da.setAcquirerTerminalNumberOwner("SBI");
		// 8.6 ex: 764 - THB
		da.setAcquirerTerminalCurrency("764");
		// 8.7 *****
		da.setTerminalSequencenumber("90000001");
		// 8.8
		da.setAcquirerTraceNumber("000000");
		// 9.1
		da.setReference1("1223313292017            ");
		// 9.2
		da.setReference2("ref4567890123456789012345");
		// 9.3
		da.setFromAccountNumber("7771345289      ");
		// 9.4
		da.setToAccountNumber("0010000032      ");
		// 9.5
		da.setOthAccountNumber("000000000000000000");
		// 10 ex. 10.50 THB
		da.setRequestAmount("000000000001050");
		// 11
		da.setCompleteAmount("000000000000000");
		// 12
		da.setFeeAmount("000000000000000");
		// 13
		da.setResponseCode("00");
		// 14
		da.setReversalCode("00");
		// 15
		da.setApproveCode("000000");
		// 16
		da.setReserve2("        ");

		String msg = da.getMessage();

		return msg;
	}
}
