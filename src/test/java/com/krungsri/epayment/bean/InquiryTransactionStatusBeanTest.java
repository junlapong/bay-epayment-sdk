package com.krungsri.epayment.bean;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.krungsri.epayment.bean.InquiryTransactionStatusBean;

public class InquiryTransactionStatusBeanTest {

	private static final Logger log = LoggerFactory.getLogger(InquiryTransactionStatusBeanTest.class);

	@BeforeClass
	public static void setup() {
		log.info("***** START *****");
	}

	@AfterClass
	public static void tearDown() {
		log.info("***** END *****");
	}

	@Test
	@Ignore
	public void shouldValidStatus() {

		InquiryTransactionStatusBean txnStatus = new InquiryTransactionStatusBean();
		txnStatus.setOrderNumber("201508050000002");
		txnStatus.setResponseCode(InquiryTransactionStatusBean.TXN_SUCCESS);
		txnStatus.setApprovalCode("666666");

		log.info(" OrderNumber: {}", txnStatus.getOrderNumber());
		log.info("    RespCode: {}", txnStatus.getResponseCode());
		log.info("ApprovalCode: {}", txnStatus.getApprovalCode());

		Assert.assertEquals(InquiryTransactionStatusBean.TXN_SUCCESS, txnStatus.getResponseCode());

	}
}
