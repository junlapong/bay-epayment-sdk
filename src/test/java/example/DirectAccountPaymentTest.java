package example;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.krungsri.epayment.bean.DirectAccountBean;
import com.krungsri.epayment.sdk.DirectAccountClient;
import com.krungsri.epayment.util.AppConfig;
import com.krungsri.epayment.util.HttpUtils;

public class DirectAccountPaymentTest {

	private static final Logger log = LoggerFactory.getLogger(DirectAccountPaymentTest.class);

	private static String SERVICE_URL;
	private static String ACQUIRER_CODE;
	private static String MERCHANT_ID;
	private static String MERCHANT_KEY;

	private static AppConfig config;
	private static ResourceBundle messsage;

	private void setup() {

		messsage = ResourceBundle.getBundle("da-message", Locale.ENGLISH);
		config = new AppConfig("config/direct-account-test.properties");

		SERVICE_URL  = config.getString("epayment.service.url");
		MERCHANT_ID  = config.getString("epayment.merchant.id");
		MERCHANT_KEY = config.getString("epayment.merchant.key");
		ACQUIRER_CODE = config.getString("epayment.acquirer.code");
		
		boolean proxyEnable = config.getBool("proxy.enable");
		
		if (Boolean.TRUE.equals(proxyEnable)) {
			HttpUtils.debugSSL(false, "ssl:handsahake");
			HttpUtils.setupProxy(config.getString("proxy.host"), config.getInt("proxy.port"));
		}
	}

	@Test
	public void shouldProcessTransaction() {
		
		log.info("***** START *****");
		
		setup();
		
		//-------------------------------------------------------------------//
		
		// Case               fromAccount / reference1
		// 71 - acc close   : 1499042217 - 000001   * DDMMYYYY
		// 75 - inactive    : 0010001768 - 000005	* DDMMYYYY
		// 78 - insuff fund : 7770058319 - 123457	* DDMMYYYY
		// 82 - db error    : 0010176810 - 700001   * DDMMYYYY
		// 00 - success     : 7770003537 - 000014   * DDMMYYYY
		// 00 - success     : 7770058480 - 123456   * DDMMYYYY
		// 99 - error       : ********** - ******   * YYYYMMDD

		// PPT                fromAccount / reference1
		//                    7771618445 - 1223313292015
		//                    7771618452 - 1223313292016

		// txnAmount
		// DECIMAL: 000000000001050 > 10.50 THB
		// AMLO   : 000000070000100 > 700,001 THB

		String orderNumber  = "18070901";                   // 8
		String toAccount    = "7770045065";                 // 10
		String fromAccount  = "7771618452";                 // 10
		String reference1   = "1223313292016            ";  // 25
		String reference2   = "REF2-00001               ";  // 25
		String txnAmount    = "000000000001050";            // 15 ex. 10.50 THB
		
		//-------------------------------------------------------------------//

		// (1) INITIATE DIRECT ACCOUNT ClIENT
		DirectAccountClient client = new DirectAccountClient();
		client.setServiceUri(SERVICE_URL);

		// (2) GET SESSION KEY
		String sessionKey = client.getSessionKey(MERCHANT_ID);
		log.info("MERCHANT ID: {}", MERCHANT_ID);
		log.info("SESSION KEY: {}", sessionKey);

		if (sessionKey == null || sessionKey.isEmpty()) {
			throw new RuntimeException("Cannot get session key.");
		}

		// (3) SEND REQUEST TO BAY
		// PAYMENT: DirectAccountBean.TXN_REQ_TYPE
		// INQUIRY: DirectAccountBean.INQ_REQ_TYPE
		
		String reqType = DirectAccountBean.INQ_REQ_TYPE;
		String reqDesc = messsage.getString("da.txn.type." + reqType);
		log.info("REQ TYPE: {} - {}", reqType, reqDesc);
		
		String requestMsg = getTransactionMessage(reqType, ACQUIRER_CODE, MERCHANT_ID, orderNumber, toAccount, fromAccount, reference1, reference2, txnAmount);
		log.debug("MSG INP: vvv\n[{}]", requestMsg);
		log.debug("MSG LEN: {} bytes", requestMsg.length());

		String decryptMsg = client.submitRequest(MERCHANT_ID, MERCHANT_KEY, sessionKey, reqType, requestMsg);

		// (4) CHECK RESPONSE MESSAGE
		String txnTypeCode = decryptMsg.substring(0, 4);
		String txnTypeDesc = messsage.getString("da.txn.type." + txnTypeCode);
		log.info("TXN TYPE: {} - {}", txnTypeCode, txnTypeDesc);

		String txnCode = decryptMsg.substring(8, 10);
		String txnDesc = messsage.getString("da.txn.code." + txnCode);
		log.info("TXN CODE: {} - {}", txnCode, txnDesc);
		//log.info("SVC TYPE: {}", decryptMsg.substring(4, 8));
		log.info("TXN DATE: {}", decryptMsg.substring(14, 22));
		log.info("TXN TIME: {}", decryptMsg.substring(22, 28));
		log.info("APPROVAL: {}", decryptMsg.substring(286, 292));

		String respStatus = client.responseStatus;
		String statusDesc = messsage.getString("da.resp.status." + respStatus);
		log.info("  STATUS: {} - {}", respStatus, statusDesc);

		String respCode = decryptMsg.substring(282, 284);
		String respDesc = messsage.getString("da.resp.code." + respCode);
		log.info("RESPONSE: {} - {}", respCode, respDesc);

		log.info("*****  END *****");
		
	}

	private String getTransactionMessage(String reqType, String acquirerCode, String merchantId, String orderNumber, String toAccount, String fromAccount, String reference1, String reference2, String txnAmount) {

		Date now = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss", Locale.ENGLISH);

		log.info("Acquirer Code : {}", acquirerCode);
		log.info("From Account  : {}", fromAccount);
		log.info("To Account    : {}", toAccount);
		log.info("OrderNumber   : {}", orderNumber);
		log.info("Reference1    : {}", reference1);
		log.info("Reference2    : {}", reference2);
		log.info("Amount (THB)  : {}", Double.valueOf(txnAmount).doubleValue() / 100);
		
		DirectAccountBean bean = new DirectAccountBean();

		// 1
		bean.setTransactionMessageType(reqType);
		// 2
		bean.setServiceType("EPAY");
		
		// 3.1
		if (DirectAccountBean.TXN_REQ_TYPE.equalsIgnoreCase(reqType)) {
			bean.setTransactionCode(DirectAccountBean.TXN_BILLPAY);
		}
		else {
			bean.setTransactionCode(DirectAccountBean.TXN_INQUIRY);
		}

		// 3.2
		bean.setFromAccountCode("00");
		// 3.3
		bean.setToAccountCode("00");
		// 4
		bean.setTransactionDate(df.format(now));
		log.debug("TransactionDate: {}", bean.getTransactionDate());
		// 5
		bean.setTransactionTime(tf.format(now));
		log.debug("TransactionTime: {}", bean.getTransactionTime());
		// 6.1
		bean.setPanLength("16");
		// 6.2
		bean.setPanNumber("025005" + fromAccount + "   ");
		// 7.1
		bean.setCvv("   ");
		// 7.2
		bean.setExpiry("      ");
		// 8.1
		bean.setPaymentType("DirectAccount");
		// 8.2 *****
		bean.setAcquirerTerminalNumber(merchantId + "       ");
		// 8.3 *****
		bean.setAcquirerTerminalLocation(acquirerCode + "                         ");
		// 8.4
		bean.setAcquirerTerminalExp10("-2");
		// 8.5
		bean.setAcquirerTerminalNumberOwner(acquirerCode);
		// 8.6
		bean.setAcquirerTerminalCurrency("764");
		// 8.7
		bean.setTerminalSequencenumber(orderNumber);
		// 8.8
		bean.setAcquirerTraceNumber("000000");
		// 9.1
		bean.setReference1(reference1);
		// 9.2
		bean.setReference2(reference2);
		// 9.3
		bean.setFromAccountNumber(fromAccount + "      ");
		// 9.4
		bean.setToAccountNumber(toAccount + "      ");
		// 9.5
		bean.setOthAccountNumber("000000000000000000");
		// 10
		bean.setRequestAmount(txnAmount);
		// 11
		bean.setCompleteAmount("000000000000000");
		// 12
		bean.setFeeAmount("000000000000000");
		// 13
		bean.setResponseCode("00");
		// 14
		bean.setReversalCode("00");
		// 15
		bean.setApproveCode("000000");
		// 16
		bean.setReserve2("        ");

		return bean.getMessage();
	}
	
}
