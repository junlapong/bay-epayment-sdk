package example;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.krungsri.epayment.sdk.InquiryTransactionStatusClient;
import com.krungsri.epayment.util.AppConfig;
import com.krungsri.epayment.util.HttpUtils;

public class InquiryTransactionStatusTest {

	private static final Logger log = LoggerFactory.getLogger(InquiryTransactionStatusTest.class);

	static String SEVICE_URL;
	static String MERCHANTNUMBER;

	static AppConfig config;
	static ResourceBundle messsage;

	private void setup() {

		messsage = ResourceBundle.getBundle("da-message", Locale.ENGLISH);
		config = new AppConfig("config/direct-account-test.properties");

		SEVICE_URL = config.getString("epayment.service.url");
		MERCHANTNUMBER = config.getString("epayment.merchant.id");

		boolean proxyEnable = config.getBool("proxy.enable");
		
		if (Boolean.TRUE.equals(proxyEnable)) {
			HttpUtils.debugSSL(false, "ssl:handsahake");
			HttpUtils.setupProxy(config.getString("proxy.host"), config.getInt("proxy.port"));
		}
	}

	@Test
	public void shouldInquiryTransactionStatus() {
		
		log.info("***** START *****");
		
		setup();

		InquiryTransactionStatusClient client = new InquiryTransactionStatusClient();
		client.setServiceUri(SEVICE_URL);

		String orderNo = "18070602";
		
		log.info("MERCHANT NO : {}", MERCHANTNUMBER);
		log.info("ORDER NUMBER: {}", orderNo);
		
		String respMessage = client.getTransactionStatus(MERCHANTNUMBER, orderNo);
		log.debug("    RESP MSG: {}", respMessage);

		Map<String, String> resp = new HashMap<String, String>();
		if (respMessage != null & respMessage.startsWith("OrderNumber=")) {

			StringTokenizer st = new StringTokenizer(respMessage, "&");
			while (st.hasMoreTokens()) {
				
				String temp = st.nextToken();
				StringTokenizer stQuery = new StringTokenizer(temp, "=");
				
				String key = stQuery.nextToken();
				String value = "";
				
				if (stQuery.hasMoreElements()) {
					value = stQuery.nextToken();
				}

				resp.put(key, value);
				log.trace("{}: {}", key, value);
			}

			String respCode = resp.get("RespCode");
			String respDesc = messsage.getString("inq.txn.status." + respCode);
			String apprvCode = resp.get("ApprovalCode");

			log.info("    RESPONSE: {} - {}", respCode, respDesc);
			log.info("APPROVE CODE: {}", apprvCode);
		}
		else {
			log.info("    RESPONSE: {}", respMessage);
		}

		log.info("***** END *****");

	}
}
