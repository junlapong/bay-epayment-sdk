/*
 *------------------------------------------------------------------------------
 * Project Name  : E-Payment
 *         Module: Tripple DES Crypto
 *
 * Created Author: Junlapong L.
 *         Date  : 2015-07-07
 *
 *------------------------------------------------------------------------------
 * Copyright (c) 2015 Bank of Ayudhya Public Company Limited.
 *==============================================================================
 */

package com.krungsri.crypto;

/**
 * 
 * Encrypt/Decrypt using 3DES EDE / ECB mode
 * 
 * @author Junlapong
 *
 */
public class TrippleDes {
	
	private static final String MODE_ENCRYPT = "T";
	private static final String MODE_DECRYPT = "F";

	/**
	 * 
	 * @param msgInput fix length message (300 bytes)
	 * @param sessionKey request from BAY server
	 * @param merchantKey provide by BAY for each merchant
	 * 
	 * @return encrypt message string
	 */
	public static String encrypt(String msgInput, String sessionKey, String merchantKey) {

		String msgEncrypt = bay.des.TripleDesClassB.DoCrypt(msgInput, sessionKey, merchantKey, MODE_ENCRYPT);

		return msgEncrypt;
	}

	/**
	 * 
	 * @param msgEncrypt encrypt message response from BAY server
	 * @param sessionKey request from BAY server
	 * @param merchantKey provide by BAY for each merchant
	 * 
	 * @return decrypt message string
	 */
	public static String decrypt(String msgEncrypt, String sessionKey, String merchantKey) {

		bay.des.TripleDesClassH des = new bay.des.TripleDesClassH();
		String msgDecrypt = des.DoCrypt(msgEncrypt, sessionKey, merchantKey, MODE_DECRYPT);
		msgDecrypt = msgDecrypt.substring(0, 300);

		return msgDecrypt;
	}
}
