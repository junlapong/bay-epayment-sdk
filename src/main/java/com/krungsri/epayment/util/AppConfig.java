package com.krungsri.epayment.util;

import java.io.InputStream;
import java.util.Properties;

public class AppConfig {

	private final String DEFAULT_CONFIG_PATH = "config/application.properties";

	private String CONFIG_PATH = null;
	private Properties properties = null;

	public AppConfig() {
		this(null);
	}

	public AppConfig(String configPath) {

		if (configPath != null) {
			CONFIG_PATH = configPath;
		}
		else {
			CONFIG_PATH = DEFAULT_CONFIG_PATH;
		}

		try {
			load();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void load() throws Exception {

		InputStream ins = null;

		try {
			ins = Thread.currentThread().getContextClassLoader().getResourceAsStream(CONFIG_PATH);
			properties = new Properties();
			properties.load(ins);
		}
		catch (Exception ex) {
			throw ex;
		}
		finally {
			if (null != ins) {
				ins.close();
			}
		}
	}

	public void reload() throws Exception {
		load();
	}

	private String getProperty(String key) {
		return properties.getProperty(key);
	}

	public String getString(String key) {
		return properties.getProperty(key);
	}

	public int getInt(String key) {
		return Integer.parseInt(getProperty(key));
	}

	public boolean getBool(String key) {
		return "true".equalsIgnoreCase(getProperty(key));
	}

}
