/*
 *------------------------------------------------------------------------------
 * Project Name  : Krungsri E-Payment SDK
 *         Module: Direct Account
 *
 * Created Author: Junlapong L.
 *         Date  : 2015-08-14
 *
 *------------------------------------------------------------------------------
 * Copyright (c) 2015 Bank of Ayudhya Public Company Limited.
 *==============================================================================
 */

package com.krungsri.epayment.bean;

import java.io.Serializable;

public class DirectAccountBean implements Serializable {

	private static final long serialVersionUID = 6872648083858415367L;

	public static String TXN_REQ_TYPE = "0200";
	public static String TXN_RESP_TYPE = "0210";
	public static String INQ_REQ_TYPE = "0700";
	public static String INQ_RESP_TYPE = "0710";

	public static String TXN_BILLPAY = "50";
	public static String TXN_INQUIRY = "35";

	// ---------------------------------------------------------------

	private String transactionMessageType;
	private String serviceType = "EPAY";
	private String paymentType = "DirectAccount";
	private String acquirerTerminalExp10 = "-2";
	private String acquirerTerminalCurrency = "764";

	private String acquirerTerminalLocation;
	private String acquirerTerminalNumber;
	private String acquirerTerminalNumberOwner;
	private String acquirerTraceNumber;
	private String approveCode;
	private String completeAmount;
	private String cvv;
	private String expiry;
	private String feeAmount;
	private String fromAccountCode = "00";
	private String fromAccountNumber;
	private String othAccountNumber;
	private String panLength;
	private String panNumber;
	private String pinBlock;
	private String reference1;
	private String reference2;
	private String requestAmount;
	private String responseCode;
	private String reversalCode;
	private String toAccountCode = "00";
	private String toAccountNumber;
	private String terminalSequencenumber;
	private String transactionCode;
	private String transactionDate;
	private String transactionTime;
	private String reserve2;

	private String _msgfixLenth = "";

	public String getAcquirerTerminalExp10() {
		return acquirerTerminalExp10;
	}

	public void setAcquirerTerminalExp10(String acquirerTerminalExp10) {
		this.acquirerTerminalExp10 = acquirerTerminalExp10;
	}

	public String getAcquirerTerminalLocation() {
		return acquirerTerminalLocation;
	}

	public void setAcquirerTerminalLocation(String acquirerTerminalLocation) {
		this.acquirerTerminalLocation = acquirerTerminalLocation;
	}

	public String getAcquirerTerminalNumber() {
		return acquirerTerminalNumber;
	}

	public void setAcquirerTerminalNumber(String acquirerTerminalNumber) {
		this.acquirerTerminalNumber = acquirerTerminalNumber;
	}

	public String getAcquirerTerminalNumberOwner() {
		return acquirerTerminalNumberOwner;
	}

	public void setAcquirerTerminalNumberOwner(String acquirerTerminalNumberOwner) {
		this.acquirerTerminalNumberOwner = acquirerTerminalNumberOwner;
	}

	public String getAcquirerTerminalCurrency() {
		return acquirerTerminalCurrency;
	}

	public void setAcquirerTerminalCurrency(String acquirerTerminalCurrency) {
		this.acquirerTerminalCurrency = acquirerTerminalCurrency;
	}

	public String getAcquirerTraceNumber() {
		return acquirerTraceNumber;
	}

	public void setAcquirerTraceNumber(String acquirerTraceNumber) {
		this.acquirerTraceNumber = acquirerTraceNumber;
	}

	public String getApproveCode() {
		return approveCode;
	}

	public void setApproveCode(String approveCode) {
		this.approveCode = approveCode;
	}

	public String getCompleteAmount() {
		return completeAmount;
	}

	public void setCompleteAmount(String completeAmount) {
		this.completeAmount = completeAmount;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(String feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getFromAccountCode() {
		return fromAccountCode;
	}

	public void setFromAccountCode(String fromAccountCode) {
		this.fromAccountCode = fromAccountCode;
	}

	public String getFromAccountNumber() {
		return fromAccountNumber;
	}

	public void setFromAccountNumber(String fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}

	public String getOthAccountNumber() {
		return othAccountNumber;
	}

	public void setOthAccountNumber(String othAccountNumber) {
		this.othAccountNumber = othAccountNumber;
	}

	public String getPanLength() {
		return panLength;
	}

	public void setPanLength(String panLength) {
		this.panLength = panLength;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPinBlock() {
		return pinBlock;
	}

	public void setPinBlock(String pinBlock) {
		this.pinBlock = pinBlock;
	}

	public String getReference1() {
		return reference1;
	}

	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}

	public String getReference2() {
		return reference2;
	}

	public void setReference2(String reference2) {
		this.reference2 = reference2;
	}

	public String getRequestAmount() {
		return requestAmount;
	}

	public void setRequestAmount(String requestAmount) {
		this.requestAmount = requestAmount;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getReversalCode() {
		return reversalCode;
	}

	public void setReversalCode(String reversalCode) {
		this.reversalCode = reversalCode;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getToAccountCode() {
		return toAccountCode;
	}

	public void setToAccountCode(String toAccountCode) {
		this.toAccountCode = toAccountCode;
	}

	public String getToAccountNumber() {
		return toAccountNumber;
	}

	public void setToAccountNumber(String toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}

	public String getTerminalSequencenumber() {
		return terminalSequencenumber;
	}

	public void setTerminalSequencenumber(String terminalSequencenumber) {
		this.terminalSequencenumber = terminalSequencenumber;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getTransactionMessageType() {
		return transactionMessageType;
	}

	/**
	 * @param transactionMessageType
	 *            <ul>
	 *            <li>0200 - DirectAccountBean.TXN_REQ_TYPE</li>
	 *            <li>0700 - DirectAccountBean.INQ_REQ_TYPE</li>
	 *            </ul>
	 */
	public void setTransactionMessageType(String transactionMessageType) {
		this.transactionMessageType = transactionMessageType;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	/**
	 * @param transactionDate
	 *            format: yyyyMMdd
	 */
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	/**
	 * @param transactionTime
	 *            format: HHmmss
	 */
	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}

	public String getReserve2() {
		return reserve2;
	}

	public void setReserve2(String reserve2) {
		this.reserve2 = reserve2;
	}

	/**
	 * @return Direct Account message in fix length format
	 */
	public String getMessage() {
		this._msgfixLenth = convertToFixLengthMessage();
		return this._msgfixLenth;
	}

	public DirectAccountBean getDataObject() {
		DirectAccountBean da = new DirectAccountBean();
		return da;
	}

	private String convertToFixLengthMessage() {

		StringBuilder msg = new StringBuilder();
		msg.append(transactionMessageType);
		msg.append(serviceType);
		msg.append(transactionCode);
		msg.append(fromAccountCode);
		msg.append(toAccountCode);
		msg.append(transactionDate);
		msg.append(transactionTime);
		msg.append(panLength);
		msg.append(panNumber);
		msg.append(cvv);
		msg.append(expiry);
		msg.append(paymentType);
		msg.append(acquirerTerminalNumber);
		msg.append(acquirerTerminalLocation);
		msg.append(acquirerTerminalExp10);
		msg.append(acquirerTerminalNumberOwner);
		msg.append(acquirerTerminalCurrency);
		msg.append(terminalSequencenumber);
		msg.append(acquirerTraceNumber);
		msg.append(reference1);
		msg.append(reference2);
		msg.append(fromAccountNumber);
		msg.append(toAccountNumber);
		msg.append(othAccountNumber);
		msg.append(requestAmount);
		msg.append(completeAmount);
		msg.append(feeAmount);
		msg.append(responseCode);
		msg.append(reversalCode);
		msg.append(approveCode);
		msg.append(reserve2);

		return msg.toString();
	}
}
