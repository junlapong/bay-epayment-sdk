package com.krungsri.epayment.bean;

import java.io.Serializable;

public class InquiryTransactionStatusBean implements Serializable {

	private static final long serialVersionUID = -4822455534763466394L;

	public static final String TXN_SUCCESS = "00";
	public static final String TXN_FAIL = "99";

	private String orderNumber;
	private String responseCode;
	private String approvalCode;

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

}
