/*
 *------------------------------------------------------------------------------
 * Project Name  : Krungsri E-Payment SDK
 *         Module: Inquiry Transaction Status Client
 *
 * Created Author: Junlapong L.
 *         Date  : 2015-08-14
 *
 *------------------------------------------------------------------------------
 * Copyright (c) 2015 Bank of Ayudhya Public Company Limited.
 *==============================================================================
 */

package com.krungsri.epayment.sdk;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.krungsri.epayment.util.HttpUtils;

public class InquiryTransactionStatusClient {

	private static final Logger log = LoggerFactory.getLogger(InquiryTransactionStatusClient.class);

	private String serviceUri;

	public String getServiceUri() {
		return this.serviceUri;
	}

	public void setServiceUri(String uri) {
		this.serviceUri = uri;
	}

	public String getTransactionStatus(String merchantId, String orderNumber) {

		log.info("get transaction status for MerchantId: {}, OrderNumber: {}", merchantId, orderNumber);

		String respMsg = null;
		String url = serviceUri + "/EPayDefaultWeb/PaymentManager/InquiryTransactionStatus.do";

		// curl command
		if (log.isDebugEnabled()) {
			StringBuilder curlCmd = new StringBuilder();
			curlCmd.append("curl -k -XPOST");
			curlCmd.append(" -d \"MERCHANTNUMBER=").append(merchantId).append("\"");
			curlCmd.append(" -d \"ORDERNUMBER=").append(orderNumber).append("\"");
			curlCmd.append(" ").append(url);
			log.debug("\n\n>{}\n", curlCmd);
		}

		try {

			StringBuilder requestMsg = new StringBuilder();
			requestMsg.append("MERCHANTNUMBER=").append(merchantId);
			requestMsg.append("&ORDERNUMBER=").append(orderNumber);
			log.debug("REQUEST MSG: {}", requestMsg);

			HttpsURLConnection conn = (HttpsURLConnection) HttpUtils.getConnection(url);
			PrintStream psOut = new PrintStream(conn.getOutputStream());
			psOut.print(requestMsg.toString());
			psOut.close();

			BufferedReader brResp = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			log.debug(" HTTP STATUS: {} {}", conn.getResponseCode(), conn.getResponseMessage());

			if (HttpsURLConnection.HTTP_OK == conn.getResponseCode()) {
				respMsg = brResp.readLine();
				log.debug("response: {}", respMsg);
			}

			brResp.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR: {}", e.getMessage());
		}

		return respMsg;
	}

}
