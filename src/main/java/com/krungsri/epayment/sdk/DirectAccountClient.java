/*
 *------------------------------------------------------------------------------
 * Project Name  : Krungsri e-Payment SDK
 *         Module: Direct Account Client
 *
 * Created Author: Junlapong L.
 *         Date  : 2015-08-14
 *
 *------------------------------------------------------------------------------
 * Copyright (c) 2015 Bank of Ayudhya Public Company Limited.
 *==============================================================================
 */

package com.krungsri.epayment.sdk;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.krungsri.crypto.TrippleDes;
import com.krungsri.epayment.bean.DirectAccountBean;
import com.krungsri.epayment.util.HttpUtils;

public class DirectAccountClient {

	private static final Logger log = LoggerFactory.getLogger(DirectAccountClient.class);

	public String messageOutput;
	public String responseStatus;

	private String serviceUri;

	public String getServiceUri() {
		return this.serviceUri;
	}

	public void setServiceUri(String uri) {
		this.serviceUri = uri;
	}

	public String getSessionKey(String merchantId) {

		log.info("get sesssion key for merchant: {}", merchantId);
		String sessionKey = "";

		try {

			String url = serviceUri + "/EPayDefaultWeb/payment/getSessionKey.do";

			// curl command
			if (log.isTraceEnabled()) {
				StringBuilder curlCmd = new StringBuilder();
				curlCmd.append("curl -k -XPOST");
				curlCmd.append(" -d \"merchantid=").append(merchantId).append("\"");
				curlCmd.append(" ").append(url);
				log.trace("\n{}\n", curlCmd);
			}

			String reqSessKeyMsg = "merchantid=" + merchantId;
			log.debug("REQUEST URL: {}", url);
			log.debug("REQUEST MSG: {}", reqSessKeyMsg);

			HttpsURLConnection conn = (HttpsURLConnection) HttpUtils.getConnection(url);
			PrintStream psOut = new PrintStream(conn.getOutputStream());
			psOut.print(reqSessKeyMsg);

			log.debug(" HTTP STATUS: {} {}", conn.getResponseCode(), conn.getResponseMessage());

			BufferedReader brResp = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String respMsg = brResp.readLine();
			log.debug("RESPONSE MSG: {}", respMsg);

			if (respMsg != null && respMsg.startsWith("merchantid=")) {
				Map<String, String> result = convertQueryStringToMap(respMsg);
				sessionKey = result.get("key");
			}

			log.debug("SESSION KEY : [{}]", sessionKey);

			psOut.close();
			brResp.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			log.error("ERROR: {}", ex.getMessage());
		}

		return sessionKey;
	}

	public String submitRequest(String merchantId, String merchantKey, String sessionKey, String msgType, String requestMsg) {

		// encrypt message from requestBean
		String enncryptMsg = TrippleDes.encrypt(requestMsg, sessionKey, merchantKey);
		log.debug("MSG ENC: vvv\n[{}]", enncryptMsg);

		// send request
		String respMsg = requestMessage(merchantId, merchantKey, sessionKey, msgType, enncryptMsg);
		log.debug("RESP ENC: vvv\n[{}]", respMsg);

		// decrypt message
		String decryptMsg = TrippleDes.decrypt(respMsg, sessionKey, merchantKey);
		log.debug("RESP DEC: vvv\n[{}]", decryptMsg);
		log.debug("RESP LEN: {} bytes", decryptMsg.length());

		return decryptMsg;
	}

	private String requestMessage(String merchantId, String merchantKey, String sessionKey, String msgType, String enncryptMsg) {

		String respMessage = "";

		try {

			String url = "";

			if (DirectAccountBean.TXN_REQ_TYPE.equals(msgType)) {
				url = serviceUri + "/EPayDefaultWeb/PaymentManager/DAPaymentInput.do";
			}
			else if (DirectAccountBean.INQ_REQ_TYPE.equals(msgType)) {
				url = serviceUri + "/EPayDefaultWeb/PaymentManager/DAPaymentInqRev.do";
			}
			else {
				throw new IllegalArgumentException("Invalid Message Type");
			}

			log.debug("REQUEST URL: {}", url);

			// curl command
			if (log.isTraceEnabled()) {
				StringBuilder curlCmd = new StringBuilder();
				curlCmd.append("curl -k -XPOST");
				curlCmd.append(" -d \"merchantnumber=").append(merchantId).append("\"");
				curlCmd.append(" -d \"key=").append(sessionKey).append("\"");
				curlCmd.append(" -d \"MessageInput=").append(enncryptMsg).append("\"");
				curlCmd.append(" ").append(url);
				log.trace("\n{}\n", curlCmd);
			}

			StringBuilder reqMsg = new StringBuilder();
			reqMsg.append("merchantnumber=").append(merchantId);
			reqMsg.append("&key=").append(sessionKey);
			reqMsg.append("&MessageInput=").append(enncryptMsg);
			log.debug("REQUEST MSG: {}", reqMsg);

			HttpsURLConnection conn = (HttpsURLConnection) HttpUtils.getConnection(url);
			PrintStream psOut = new PrintStream(conn.getOutputStream());
			psOut.print(reqMsg.toString());
			psOut.close();

			int httpCode = conn.getResponseCode();
			log.info(" HTTP STATUS: {} {}", httpCode, conn.getResponseMessage());

			BufferedReader brResp = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			if (HttpsURLConnection.HTTP_OK == httpCode) {

				String respMsg = brResp.readLine();
				log.debug("FULL RESP LEN: {}", respMsg.length());
				log.debug("FULL RESP MSG: {}", respMsg);

				if (respMsg != null) {

					int start = respMsg.indexOf("key=");
					respMsg = respMsg.substring(start);

					Map<String, String> result = convertQueryStringToMap(respMsg);
					respMessage = result.get("MessageOutput");

					this.responseStatus = result.get("STATUS");
					log.info(" RESP STATUS: {}", result.get("STATUS"));
				}
			}

			brResp.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR: {}", e.getMessage());
		}

		return respMessage;
	}

	private Map<String, String> convertQueryStringToMap(String respMsg) {

		Map<String, String> result = new HashMap<String, String>();
		StringTokenizer query = new StringTokenizer(respMsg, "&");

		while (query.hasMoreTokens()) {

			String temp = query.nextToken();
			StringTokenizer token = new StringTokenizer(temp, "=");

			while (token.hasMoreTokens()) {

				String key = token.nextToken();
				String value = "";
				if (token.hasMoreTokens()) {
					value = token.nextToken();
				}

				log.trace("{}:{}", key, value);

				result.put(key, value);
			}
		}

		return result;
	}
}
