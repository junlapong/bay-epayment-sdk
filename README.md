# Krungsri Direct Account SDK

## Functional
1. Direct Account
2. Inquiry Transaction Status

## Prerequisites
1. Java SDK version 1.8 (Oracle & OpenJDK or IBM JDK version 1.6 SR10+)
- [Diagnosing TLS, SSL, and HTTPS](https://blogs.oracle.com/java-platform-group/entry/diagnosing_tls_ssl_and_https)

## Run with Maven
### (1) Install TripleDes-1.0.jar to local maven repo
```
mvn install:install-file -Dfile=lib/TripleDes-1.0.jar -DgroupId=bay.des -DartifactId=TripleDes -Dversion=1.0 -Dpackaging=jar
```

### (2) Add TripleDes to pom.xml
```xml
<dependencies>
    <dependency>
        <groupId>bay.des</groupId>
        <artifactId>TripleDes</artifactId>
        <version>1.0</version>
    </dependency>
</dependencies>
```

### (3) Test and build

```
mvn clean test
```

### (4) Install bay-epayment-sdk to local maven repo

```
mvn clean install -Dmaven.test.skip
```

### (5) Add bay-epayment-sdk to maven project

Add `bay-epayment-sdk` in `pom.xml`

```xml
<dependencies>
    <dependency>
        <groupId>bay.epayment.sdk</groupId>
        <artifactId>bay-epayment-sdk</artifactId>
        <version>1.0</version>
    </dependency>
</dependencies>
```

## Example
Please see example `src/test/java/example/DirectAccountPaymentTest.java`

### Testing Configuration

create configuration file for test at `src/test/resources/config/direct-account-test.properties`

```
##### TEST #####
epayment.service.url=https://int.krungsriepayment.com
##### PROD #####
#epayment.service.url=https://www.krungsriepayment.com

### SID  ###
epayment.acquirer.code=SID
epayment.merchant.id=<MERCHANT_ID>
epayment.merchant.key=<MERCHANT_KEY>

##### PROXY #####
proxy.enable=false
proxy.host=127.0.0.1
proxy.port=3128
proxy.auth=false
proxy.user=username
proxy.pass=password
```
